import React from "react";
import { mount } from "@cypress/react";
import Information from "../Information";
import "bootstrap/dist/css/bootstrap.min.css";

const data = [
  {
    2000: 1.2,
    average: 0.5,
    min: 0.1,
    max: 1,
    stateIndex: 0,
  },
];
const selectedStateIndex = 0;
const selectedYear = 2000;

beforeEach(
  () => void mount(<Information data={data} selectedStateIndex={selectedStateIndex} selectedYear={selectedYear} />)
);

describe("Muestra la información correcta.", () => {
  it("Year selected", () => {
    cy.get("[data-testid=selectedYear]").contains("1.2");
    cy.get("[data-testid=selectedYearTitle]").contains("Año " + selectedYear);
  });
  it("Max", () => cy.get("[data-testid=max]").contains("1"));
  it("Min", () => cy.get("[data-testid=min]").contains("0.1"));
  it("Promedio", () => cy.get("[data-testid=average]").contains("0.5"));
  it("Título", () => cy.get("[data-testid=title]").contains("IDH de Aguascalientes"));
});
