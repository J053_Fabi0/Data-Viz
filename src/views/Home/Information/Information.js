import React from "react";
import { Table } from "react-bootstrap";
const { states } = require("../../../utils/constants");

export default function Información({ data, selectedStateIndex, selectedYear }) {
  const { min, max, average, [selectedYear]: year } = data[selectedStateIndex];

  return (
    <div className="data mt-2" data-testid="information">
      <Table striped bordered>
        <thead>
          <tr>
            <th data-testid="title" colSpan="2">
              IDH de {states.names[selectedStateIndex]}
            </th>
          </tr>
        </thead>

        <tbody>
          <tr>
            <td data-testid="selectedYearTitle">{"Año " + selectedYear}</td>
            <td data-testid="selectedYear">{year}</td>
          </tr>
          <tr>
            <td>Promedio</td>
            <td data-testid="average">{average}</td>
          </tr>
          <tr>
            <td>Menor</td>
            <td data-testid="min">{min}</td>
          </tr>
          <tr>
            <td>Mayor</td>
            <td data-testid="max">{max}</td>
          </tr>
        </tbody>
      </Table>
    </div>
  );
}
