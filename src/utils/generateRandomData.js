const random = require("./randomNumbers");

/**
 * Generar un intervalo al azar de años, en un rango dado y con una longitud determinada.
 */
const generateRandomYearInterval = (numberOfYears = 22, minYear = 2000, maxYear = new Date().getFullYear()) => {
  const randomEarlierYear = random(minYear + numberOfYears - 1, maxYear);
  return { olderYear: randomEarlierYear - numberOfYears + 1, earlierYear: randomEarlierYear };
};

/**
 * Generar los datos al azar que alimentarán la gráfica.
 */
const generateRandomData = () => {
  const minIDHPosible = 0.05 * 100,
    maxIDHPosible = 1 * 100;
  const data = {};

  // Genero el intervalo al azar de años.
  const { olderYear, earlierYear } = generateRandomYearInterval();
  data.years = Array(earlierYear - olderYear + 1)
    .fill(0)
    .map((_, i) => olderYear + i);

  data.data = [];
  for (let i = 0; i < 32; i++) {
    data.data[i] = {};
    const idhs = [];
    for (let year = olderYear; year <= earlierYear; year++) {
      const randomIDH = random(minIDHPosible, maxIDHPosible) / 100;
      data.data[i][year] = randomIDH;
      idhs.push(randomIDH);
    }

    data.data[i].stateIndex = i;
    data.data[i].min = Math.min(...idhs);
    data.data[i].max = Math.max(...idhs);
    data.data[i].average = +(idhs.reduce((p, c) => p + c, 0) / idhs.length).toFixed(2);
  }

  return data;
};

module.exports = generateRandomData;
